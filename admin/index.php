<?php
	// Dashboard Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';
	require_once '../models/User_model.php';

	require_once '../library/DB.php';

	$user = new User_model();

	// Login Panel
	if (isset($_POST['login'])) {

		$username = $_POST['username'];
		$password = $_POST['password'];
		 // dump($_POST, TRUE);
		// Check Valid Admin or Not
		if($user->validate_user_login($username, $password)) {

			// Session Start
			session_start();

			// Set Session Value
			$_SESSION['username'] = $username;
			$_SESSION['logged_in'] = TRUE;
			// dump($_SESSION, TRUE);
			

			// Redirect to Dashboard Page
			$url = '../dashboard/dashboard.php';
			header('Location: '.$url);
			// redirect($url);

		}
		else {
			$msg = "Opps! Incorrect Login credentials!";
		}
	}

	// Load dashbord view file
	include_once '../views/admin/index_view.php';
?>



		

		