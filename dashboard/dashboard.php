<?php
	// Dashboard Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';
	
	require_once '../models/User_model.php';

	// Check User LoggedIn or Not
	// $user = new User_model();

	if ( !DB::is_logged_in() ) {
		$url = $config['base_url'].'admin/index.php';
		redirect($url);
		die();
	}


	// Load dashbord view file
	include_once '../views/dashboard/dashboard_view.php';
?>



		

		