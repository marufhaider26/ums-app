<?php include_once '../views/layout/header_view.php'; ?>

	<section class="main-content">
		<div class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<a href="<?php echo $config['base_url']; ?>user/index.php" class="btn btn-info">+ User List</a>
						<h3>User Entry</h3>

						<form action="" method="POST" class="form-horizontal">
							
							<?php
								// Show Success/Error message
							
								if (isset($msg)) {
							?>
								<div class="form-group">
									<label for="name" class="col-sm-2 control-label">&nbsp;</label>
									<div class="col-sm-offset-1 col-sm-6">

										<div class="alert alert-success">
											<?php echo (isset($msg)) ? $msg : ''; ?>
										</div>
									</div>
								</div>
							<?php
								}
							?>
							

							<div class="form-group">
								<label for="name" class="col-sm-2 control-label">Name</label>
								<div class="col-sm-offset-1 col-sm-6">
									<input type="hidden" class="form-control" id="id" name="id" value="<?php echo $data['id']; ?>">
									<input type="text" class="form-control" id="name" name="name" value="<?php echo $data['name']; ?>" required>
								</div>
							</div>

							<div class="form-group">
								<label for="username" class="col-sm-2 control-label">User Name</label>
								<div class="col-sm-offset-1 col-sm-6">
									<input type="text" class="form-control" id="username" name="username" value="<?php echo $data['username']; ?>" required>
								</div>
							</div>

							<div class="form-group">
								<label for="email" class="col-sm-2 control-label">Email</label>
								<div class="col-sm-offset-1 col-sm-6">
									<input type="email" class="form-control" id="email" name="email" value="<?php echo $data['email']; ?>" required>
								</div>
							</div>

							<div class="form-group">
								<label for="password" class="col-sm-2 control-label">Password</label>
								<div class="col-sm-offset-1 col-sm-6">
									<input type="password" class="form-control" id="password" name="password" value="<?php echo $data['password']; ?>" required>
								</div>
							</div>

							<div class="form-group">
								<label for="contact" class="col-sm-2 control-label">Contact</label>
								<div class="col-sm-offset-1 col-sm-6">
									<input type="text" class="form-control" id="contact" name="contact" value="<?php echo $data['contact']; ?>" required>
								</div>
							</div>

							
							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-6">
									<button type="submit" name="edit" class="btn btn-success">Update</button>
								</div>
							</div>
						</form>

						
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include_once '../views/layout/footer_view.php'; ?>