<?php include_once '../views/layout/header_view.php'; ?>

<!-- Main Section -->
	<section class="row main-content">
		<div class="col-md-12">
			<div class="user-content">
				<h1>Users:</h1>
				<a href="<?php echo $config['base_url']?>user/index.php" class="btn btn-success">User List</a>
				<hr>
				
				<form class="form-horizontal" method="POST" action="">
				  	
				  	<?php
						// Show Success/Error message
						
						if (isset($msg)) {
					?>
						<div class="form-group">
							<label for="name" class="col-sm-2 control-label">&nbsp;</label>
							<div class="col-sm-offset-1 col-sm-6">

								<div class="alert alert-success">
									<?php echo (isset($msg)) ? $msg : ''; ?>
								</div>
							</div>
						</div>
					<?php
						}
					?>



				  	<div class="form-group">
				    	<label for="name" class="col-sm-4 control-label">Name</label>
					    <div class="col-sm-4">
					      <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name">
					    </div>
				  	</div>
				  	<div class="form-group">
				    	<label for="username" class="col-sm-4 control-label">Username</label>
					    <div class="col-sm-4">
					      <input type="text" class="form-control" id="username" name="username" placeholder="Enter Username">
					    </div>
				  	</div>

				  	<div class="form-group">
				    	<label for="password" class="col-sm-4 control-label">Password</label>
					    <div class="col-sm-4">
					      <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password">
					    </div>
				  	</div>
				  	<div class="form-group">
				    	<label for="email" class="col-sm-4 control-label">Email</label>
					    <div class="col-sm-4">
					      <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email">
					    </div>
				  	</div>
				  	<div class="form-group">
				    	<label for="contact" class="col-sm-4 control-label">Mobile No.</label>
					    <div class="col-sm-4">
					      <input type="text" class="form-control" id="contact" name="contact" placeholder="Enter Mobile No.">
					    </div>
				  	</div>
				  	<div class="form-group">
				    	<label for="file" class="col-sm-4 control-label">Picture</label>
					    <div class="col-sm-4">
					      <input type="file" class="" id="file" name="file" >
					    </div>
				  	</div>


				  	
				 
				  	<div class="form-group">
				    	<div class="col-sm-offset-4 col-sm-10">
				      		<button type="submit" name="create" class="btn btn-success">Submit</button>
				    	</div>
				  	</div>
				</form>

			</div>
		</div>
	</section>

<?php include_once '../views/layout/footer_view.php'; ?>