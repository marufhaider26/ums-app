<?php
	session_start();
	dump($_SESSION);
?>

<!DOCTYPE html>
<html>
<head>
	<title>UMS - Dashboard</title>

	<link rel="stylesheet" type="text/css" href="<?php echo $config['base_url']?>assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $config['base_url']?>assets/css/style.css">
</head>
<body>
	<!-- Page wrapper -->
	<div class="container-fluid">
		<!-- header Section -->
		<header class="row">
			<div class="col-md-12">
				<nav class="navbar navbar-inverse">
				  <div class="container-fluid">
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="<?php echo $config['base_url']?>dashboard/dashboard.php">UMS</a>
				    </div>

				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li class="active"><a href="<?php echo $config['base_url']?>dashboard/dashboard.php">Dashboard <span class="sr-only">(current)</span></a></li>
				        <li><a href="<?php echo $config['base_url']?>user/index.php">User</a></li>
				      </ul>
				      
				      <ul class="nav navbar-nav navbar-right">
				        <li><a href="#">Logged in as <?php echo isset($_SESSION['username'])?$_SESSION['username']: ''; ?></a></li>
				        <li><a href="<?php echo $config['base_url']?>admin/index.php"">Logout</a></li>
				      </ul>
				    </div>
				  </div>
				</nav>
			</div>
		</header>