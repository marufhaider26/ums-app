
<?php

	// Load core required file
	require_once '../library/DB.php';
	/**
	* 
	*/
	class User_model 
	{
		protected $table = 'tbl_users';

		private $name;
		private $email;
		private $username;
		private $password;
		private $contact;
		private $file = '';
		
		// Set Name
		public function setName($name) {
			$this->name = $name;
		}

		// Set Email
		public function setEmail($email) {
			$this->email = $email;
		}

		// Set Username
		public function setUsername($username) {
			$this->username = $username;
		}

		// Set Password
		public function setPassword($password) {
			$this->password = $password;
		}

		// Set Contact
		public function setContact($contact) {
			$this->contact = $contact;
		}


		// Save New User 
		public function save_user() {

			$query = "INSERT INTO $this->table(name, email, username, password, contact, file) VALUES( '$this->name', '$this->email', '$this->username', '$this->password', '$this->contact', '$this->file')";
			// dump($query, TRUE);
			return DB::insert($query);
		}

		// Fetch User Data 
		public function get_users()
		{
			return DB::select_all($this->table);
		}


		// Fetch User Data by ID
		public function get_user_by_id($id)
		{
			return DB::select_by_id($id, $this->table);
		}

		// Update User
		public function update_user($id) {

			$query = "UPDATE $this->table SET name='$this->name', email='$this->email', username='$this->username', password='$this->password', contact='$this->contact' WHERE id='$id'";

			return DB::update($query);
		}

		// Delete User by ID
		public function del_user($id)
		{
			return DB::delete($id, $this->table);
		}

		// Check valid User or Not
		public function validate_user_login($username, $password) {
			$data = DB::select_by_un_pass($username, $password);
			// dump($data, TRUE);
			return ($username === $data['username'] && $password === $data['password']);
		}
	}