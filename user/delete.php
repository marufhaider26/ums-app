<?php


	// User Create Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';
	require_once '../models/User_model.php';

	// Delete user
	$user = new User_model(); 

	if(isset($_GET['action']) && $_GET['action']=='delete') {
		$id = (int)$_GET['id'];

		if($user->del_user($id)) {

			$msg = 'Data Deleted Successfully!';
			$url = $config['base_url'].'user/index.php?msg='.urlencode($msg);

			redirect($url);
		}
		else {
			$msg = 'Something Wrong Here!!!';
		}

	}