<?php
	// User Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';

	require_once '../models/User_model.php';

	// Fetch User data
	$user = new User_model(); 
	if(isset($_GET['action']) && $_GET['action']=='edit') {
		$id = (int)$_GET['id'];
		$data = $user->get_user_by_id($id);

		// dump($data);
	}

	// Update User data
	if (isset($_POST['edit'])) {

		$id 		= $_GET['id'];
		// $id 		= $_POST['id'];
		$name 		= $_POST['name'];
		$username 	= $_POST['username'];
		$email 		= $_POST['email'];
		$password 	= $_POST['password'];
		$contact 	= $_POST['contact'];

		$user->setName($name);
		$user->setUsername($username);
		$user->setEmail($email);
		$user->setPassword($password);
		$user->setContact($contact);

		if ($user->update_user($id)) {
			$msg = '<span style="color: #06960E; font-weight: bold;"> Data Updated Successfully! </sapn>';
		}
		else {
			$msg = '<span style="color: #06960E; font-weight: bold;"> Something Wrong Here!!! </sapn>';
		}
	}

	// View File
	require '../views/user/edit_view.php';