<?php
	// User Create Controller
	include_once '../config/config.php';
	include_once '../helpers/core_helper.php';
	require_once '../models/User_model.php';

	$user = new User_model();

	if (isset($_POST['create'])) {

		// dump($_POST, TRUE);
		$name = $_POST['name'];
		$username = $_POST['username'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$contact = $_POST['contact'];

		$user->setName($name);
		$user->setUsername($username);
		$user->setEmail($email);
		$user->setPassword($password);
		$user->setContact($contact);

		if ($user->save_user()) {

			$msg = '<span style="color: #06960E; font-weight: bold;"> Data Inserted Successfully! </sapn>';

			// $url = "index.php";
			// redirect($url);
		}
		else {
			$msg = '<span style="color: #06960E; font-weight: bold;"> Something Wrong Here!!! </sapn>';
		}

	}

	// Load User create view file
	include_once '../views/user/create_view.php';
?>



		

		