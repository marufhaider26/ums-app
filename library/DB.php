<?php 
	require_once '../config/db_config.php';
	/**
	 * DB Class for manipulating data in database.
	 *
	 * @category    Libraries
	 * @author      Shaquib Mahmud
	 * @link        http://www.sqbctgbd.com
	 */
	class DB
	{
		public static $link;
		public $error;

		function __construct()
		{
			self::$link = self::connection();
		}

		/**
		 * Database connection
		 *
		 * @return 		obj
		 */
		
		public static function connection() {

			if (!isset(self::$link)) {
				return new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
				
			}

			if(self::$link === FALSE) {
				$this->error = "Connection fail" . self::$link->connect_error;
				return false;
			}
		}

		/**
		 * SELECT ALL DATA
		 *
		 * @param       string  $table    Input table name
		 * @return 		array
		 */

		public static function select_all($table) {
			
			$query = "SELECT * FROM $table";

			$result = self::connection()->query($query);

			if ($result->num_rows > 0) {
				while($row = $result -> fetch_assoc()) {
					$records[] = $row;
				}

				$result -> free();
				return $records;
			}
			else {
				return false;
			}
		}

		/**
		 * Select Data BY ID
		 *
		 * @param       number  $id 	  Input ID
		 * @param       string  $table    Input table name
		 * @return 		array
		 */

		public static function select_by_id($id, $table) {

			$query = "SELECT * FROM $table WHERE id=$id";

			$result = self::connection()->query($query);
			
			if ($result->num_rows > 0) {
				return $result->fetch_assoc();
			}
			else {
				return false;
			}
		}

		/**
		 * Select Data BY Username and Password
		 *
		 * @param       string  $username 	  Input Username
		 * @param       string  $password 	  Input Password
		 * @return 		array
		 */

		public static function select_by_un_pass($username, $password) {

			$query = "SELECT * FROM tbl_users WHERE username='$username' AND password='$password'";
			// dump($query, TRUE);
			$result = self::connection()->query($query);
			
			if ($result->num_rows > 0) {
				return $result->fetch_assoc();
			}
			else {
				return false;
			}
		}

		/**
		 * Insert Data
		 *
		 * @param       string  $query 	  Input Query
		 * @return 		bool
		 */

		public static function insert($query) {

			$insert_row = self::connection()->query($query);

			if (!$insert_row) {
				die("Error :(".self::connection()->errno.")".self::connection()->error);
			}

			return $insert_row;
		}

		/**
		 * Update Data
		 *
		 * @param       string  $query 	  Input Query
		 * @return 		bool
		 */

		public static function update($query) {

			$update_row = self::connection()->query($query);

			if (!$update_row) {
				die("Error :(".self::connection()->errno.")".self::connection()->error);
			}

			return $update_row;
		}


		/**
		 * Delete Data
		 *
		 * @param       number  $id 	  Input ID
		 * @param       string  $table    Input table name
		 * @return 		bool
		 */

		public static function delete($id, $table) {

			$query = "DELETE FROM $table WHERE id=$id";

			$delete_row = self::connection()->query($query);

			if (!$delete_row) {
				die("Error :(".self::connection()->errno.")".self::connection()->error);
			}

			return $delete_row;
		}


		/**
		 * Escape Data
		 *
		 * @param       string  $input    Input data
		 * @return 		bool
		 */
		public static function escape($input) {
			$str = self::connection()->real_escape_string($input);
			$escapeStr = htmlspecialchars(htmlentities($str));
			return $escapeStr;
		}


		/**
		 * SELECT ALL DATA from Multiple Table (Joining)
		 *
		 * @param       string  $table    Input table name
		 * @return 		array
		 */

		public static function custom_query($query) {
			
			// $query = "SELECT * FROM $table";

			$result = self::connection()->query($query);

			if ($result->num_rows > 0) {
				while($row = $result -> fetch_assoc()) {
					$records[] = $row;
				}

				$result -> free();
				return $records;
			}
			else {
				return false;
			}
		}

		// Check LoggedIn or Not
		public static function is_logged_in() {

			if ($_SESSION['username'] == NULL &&  $_SESSION['logged_in']!= TRUE) {

				return false;
			}
			else {
				return true;
			}
		}

	}